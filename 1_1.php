<?php

$a=2;
$b=4;
$resultados=[];
$resultados["suma"]=$a+$b;
$resultados["resta"]=$a-$b;
$resultados["producto"]=$a*$b;
$resultados["division"]=$a/$b;
$resultados["raiz cuadrada"]= sqrt($a);
$resultados["potencia"]=$a**$b;

// mostrando los resultados sin foreach
echo "Suma: {$resultados["suma"]} <br>";
echo "Resta: {$resultados["resta"]} <br>";
echo "Producto: {$resultados["producto"]} <br>";
echo "Cociente: {$resultados["division"]} <br>";
echo "Raiz cuadrada {$resultados["raiz cuadrada"]} <br>";
echo "Potencia: {$resultados["potencia"]} <br>";

// mostrando los resultados utilizando foreach
foreach($resultados as $operacion=>$resultado){
    echo "{$operacion}: {$resultado}<br>";
}
